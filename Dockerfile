FROM registry.redhat.io/ubi8/ubi

ADD cassandra.repo /etc/yum.repos.d/

RUN dnf install cassandra -y && dnf clean all && systemctl enable cassandra
